# Use Node.js version 14 as the base image
FROM node:18.17

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Expose the port that your NestJS application will run on
EXPOSE 3000

# Command to run your NestJS application
CMD ["npm", "run", "start"]