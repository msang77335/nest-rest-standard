import * as winston from 'winston';
import { LoggerInfo } from './winstion.interface';
import { toPrettyJson } from 'src/common/helpers';

const winstonInstance = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json(),
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'logs/app.log' }),
  ],
});

export default class Logger {
  static info(info: LoggerInfo) {
    winstonInstance.info(toPrettyJson(info));
  }
  static error(info: LoggerInfo) {
    winstonInstance.error(toPrettyJson(info));
  }
}
