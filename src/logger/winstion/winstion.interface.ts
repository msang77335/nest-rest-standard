export interface LoggerInfo {
  body?: object;
  query?: object;
  url: string;
  method: string;
  requestId: string;
  type: 'Response' | 'Request';
  response?: object;
}
