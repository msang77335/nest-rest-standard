import { Prop } from '@nestjs/mongoose';

export abstract class BaseSchema {
  @Prop({ required: true })
  created_at: Date;

  @Prop({ required: true })
  updated_at: Date;

  constructor() {
    this.created_at = new Date();
    this.updated_at = new Date();
  }
}
