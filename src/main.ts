import './common/helpers/general.helper';
import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LoggingInterceptor } from './common/interceptors';
import { AllExceptionsFilter, HttpExceptionFilter } from './common/filters';
import { CustomValidationPipe } from './common/pipes';
import { MongooseExceptionFilter } from './common/filters/mongoose-exception.filter';
import { ConfigService } from '@nestjs/config';
import { HttpEnum } from './enums';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const configService = app.get(ConfigService);

  app.useGlobalPipes(new CustomValidationPipe());
  app.useGlobalInterceptors(new LoggingInterceptor());

  const adapterHost = app.get(HttpAdapterHost);
  app.useGlobalFilters(
    new AllExceptionsFilter(adapterHost),
    new MongooseExceptionFilter(),
    new HttpExceptionFilter(),
  );

  await app.listen(configService.get(HttpEnum.PORT));
}
bootstrap();
