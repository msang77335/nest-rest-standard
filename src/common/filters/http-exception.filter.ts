import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { ValidationException } from '../exceptions';
import { httpFilters } from '../helpers';
import Logger from 'src/logger/winstion';

@Catch(HttpException, ValidationException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException | ValidationException, host: ArgumentsHost) {
    const { method, url, response, requestId } = httpFilters(host);

    const exceptionStatus = exception.getStatus();
    const exceptionResponse = exception.getResponse();

    let res = {};
    if (typeof exceptionResponse === 'string') {
      res = {
        message: exceptionResponse,
        statusCode: HttpStatus.BAD_REQUEST,
      };
    } else {
      res = { ...exceptionResponse };
    }

    Logger.error({ url, method, requestId, type: 'Response', response: res });

    response.status(exceptionStatus).json(res);
  }
}
