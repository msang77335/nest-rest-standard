import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
  InternalServerErrorException,
} from '@nestjs/common';
import { MongooseError } from 'mongoose';
import { httpFilters } from '../helpers';
import Logger from 'src/logger/winstion';

@Catch(MongooseError)
export class MongooseExceptionFilter implements ExceptionFilter {
  catch(exception: MongooseError, host: ArgumentsHost) {
    const { method, url, response, requestId } = httpFilters(host);

    const httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    Logger.error({
      url,
      method,
      requestId,
      type: 'Response',
      response: { message: exception?.message },
    });

    response.status(httpStatus).json({
      error: new InternalServerErrorException().message,
      statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
    });
  }
}
