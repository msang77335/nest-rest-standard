import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpStatus,
  InternalServerErrorException,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { httpFilters } from '../helpers';
import Logger from 'src/logger/winstion';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  catch(exception: unknown, host: ArgumentsHost): void {
    // In certain situations `httpAdapter` might not be available in the
    // constructor method, thus we should resolve it here.
    const { httpAdapter } = this.httpAdapterHost;
    const { method, url, requestId } = httpFilters(host);

    const ctx = host.switchToHttp();

    const httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    const response = new InternalServerErrorException();

    let res = {};
    if (typeof exception === 'string') {
      res = {
        message: exception,
        error: new InternalServerErrorException().name,
        statusCode: HttpStatus.BAD_REQUEST,
        requestId,
      };
    } else if (typeof exception === 'object') {
      res = { ...exception, requestId };
    } else {
      res = exception;
    }

    Logger.error({ url, method, requestId, type: 'Response', response: res });

    httpAdapter.reply(ctx.getResponse(), response, httpStatus);
  }
}
