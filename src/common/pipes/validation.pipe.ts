import { Injectable, ValidationPipe } from '@nestjs/common';
import { getMessageError } from '../helpers';
import { ValidationException } from '../exceptions';

@Injectable()
export class CustomValidationPipe extends ValidationPipe {
  constructor() {
    super({
      exceptionFactory: (errors) => {
        const message = getMessageError(errors);
        return new ValidationException(message);
      },
      stopAtFirstError: true,
    });
  }
}
