import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import Logger from 'src/logger/winstion';
import { getRequestId } from '../helpers';

@Injectable()
export class LoggingMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const { method, body, query, originalUrl } = req;

    const requestId = getRequestId(req);

    Logger.info({
      url: originalUrl,
      method,
      requestId,
      type: 'Request',
      query,
      body,
    });

    next();
  }
}
