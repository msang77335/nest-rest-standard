import { ArgumentsHost, ExecutionContext } from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthEnum } from 'src/enums';

export function getRequestId(req) {
  const id =
    req.headers[AuthEnum.X_REQUEST_ID] || AuthEnum.INVALID_X_REQUEST_ID;

  let requestId = '';

  if (typeof id === 'string' && id) {
    requestId = id;
  } else {
    requestId = AuthEnum.INVALID_X_REQUEST_ID;
  }
  return requestId;
}

export function httpFilters(host: ArgumentsHost) {
  const ctx = host.switchToHttp();
  const response = ctx.getResponse<Response>();
  const request = ctx.getRequest<Request>();

  const method = request.method;
  const url = request.url;

  const requestId = getRequestId(request);

  return { method, url, request, response, requestId };
}

export function httpInterceptors(context: ExecutionContext) {
  const request = context.switchToHttp().getRequest();

  const method = request.method;
  const url = request.url;

  // Log request body, params, and query
  const body = request.body;
  const params = request.params;
  const query = request.query;
  const requestId = getRequestId(request);

  return { method, url, request, body, params, query, requestId };
}
