import { ValidationError } from '@nestjs/common';

export function getMessageError(errors: ValidationError[]): string {
  if (errors[0].children.length === 0)
    return `${errors[0].constraints[Object.keys(errors[0].constraints)[0]]}`;

  return `${errors[0].property}.${getMessageError(errors[0].children)}`;
}
