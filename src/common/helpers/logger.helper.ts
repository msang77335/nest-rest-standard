import { isObjectEmpty } from './general.helper';

export function toPrettyJson(fields?: any) {
  const log = {};
  if (fields) {
    for (const [key, value] of Object.entries(fields)) {
      if (typeof value === 'string') log[key] = value;
      else if (value && typeof value === 'object' && !isObjectEmpty(value)) {
        log[key] = toPrettyJson(value);
      }
    }
  }
  return log;
}
