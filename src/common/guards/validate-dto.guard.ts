import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { getMessageError } from '../helpers';
import { ValidationException } from '../exceptions';

@Injectable()
export class ValidateDTOGuard<T extends object> implements CanActivate {
  constructor(private readonly dtoClass: new (...args: any[]) => T) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    // Transform request body into DTO instance
    const dto = plainToClass(this.dtoClass, request.body);

    // Validate DTO
    const errors = await validate(dto, { stopAtFirstError: true });
    if (errors.length > 0) {
      // Throw an exception if validation fails
      const message = getMessageError(errors);
      throw new ValidationException(message);
    }

    // Validation passed
    return true;
  }
}
