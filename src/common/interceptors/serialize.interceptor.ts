import {
  UseInterceptors,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs';
import { plainToClass } from 'class-transformer';

interface ClassConstructor {
  new (...args: any[]): object;
}

export function Serialize(dtoRes: ClassConstructor, dtoReq?: ClassConstructor) {
  return UseInterceptors(new SerializeInterceptor(dtoRes, dtoReq));
}

export class SerializeInterceptor implements NestInterceptor {
  constructor(
    private dtoRes: ClassConstructor,
    private dtoReq: ClassConstructor,
  ) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const http = context.switchToHttp();
    const request = http.getRequest();

    // Transform incoming request payload
    if (request.body) {
      const transformedRequest = plainToClass(this.dtoReq, request.body, {
        excludeExtraneousValues: true,
      });
      http.getRequest().body = transformedRequest;
    }

    return next.handle().pipe(
      map((data: ClassConstructor) => {
        const dataOb = JSON.parse(JSON.stringify(data));
        return plainToClass(this.dtoRes, dataOb, {
          excludeExtraneousValues: true,
        });
      }),
    );
  }
}
