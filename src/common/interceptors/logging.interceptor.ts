import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { httpInterceptors } from '../helpers';
import Logger from 'src/logger/winstion';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const { url, method, requestId } = httpInterceptors(context);

    return next.handle().pipe(
      tap((response) => {
        Logger.info({
          url,
          method,
          requestId,
          type: 'Response',
          response,
        });
      }),
    );
  }
}
