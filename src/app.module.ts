import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { SampleModule } from './shared/sample/sample.module';
import { DatabaseModule } from './database/database.module';
import { BaseModule } from './base';

@Module({
  imports: [BaseModule, UserModule, SampleModule, DatabaseModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
