import { Module } from '@nestjs/common';
import { ExampleMongodbModule } from './mongodb/example-db.module';

@Module({
  imports: [ExampleMongodbModule],
})
export class DatabaseModule {}
