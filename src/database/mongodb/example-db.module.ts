import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { MongoEnum } from 'src/enums';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: (config: ConfigService) => {
        const dbName = config.get(MongoEnum.MONGO_DB_NAME);
        const user = config.get(MongoEnum.MONGO_USER);
        const password = config.get(MongoEnum.MONGO_PASSWORD);
        const host = config.get(MongoEnum.MONGO_HOST);
        const port = config.get(MongoEnum.MONGO_PORT);

        return {
          uri: `mongodb://${host}:${port}`,
          dbName,
          auth: {
            username: user,
            password: password,
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
})
export class ExampleMongodbModule {}
