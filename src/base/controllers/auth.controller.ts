import { Controller, Post, UseGuards } from '@nestjs/common';

import { AuthService } from 'src/auth';
import { LocalSignDTOReq, LocalSignDTORes } from 'src/auth/auth.dto';
import { User } from 'src/auth/decorators';
import { LocalAuthGuard } from 'src/auth/guards';
import { ValidateDTOGuard } from 'src/common/guards';
import { Serialize } from 'src/common/interceptors';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('sign')
  @UseGuards(new ValidateDTOGuard(LocalSignDTOReq), LocalAuthGuard)
  @Serialize(LocalSignDTORes, LocalSignDTOReq)
  public login(@User() user) {
    return this.authService.localSign(user);
  }
}
