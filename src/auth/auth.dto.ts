import { Expose } from 'class-transformer';
import { IsEmail, IsNotEmpty, MinLength, NotContains } from 'class-validator';

export class LocalSignDTOReq {
  @IsEmail()
  @IsNotEmpty()
  @Expose()
  email: string;

  @IsNotEmpty()
  @MinLength(6)
  @NotContains(' ')
  @Expose()
  password: string;
}

export class LocalSignDTORes {
  @Expose()
  access_token: string;

  @Expose()
  refresh_token: string;
}
