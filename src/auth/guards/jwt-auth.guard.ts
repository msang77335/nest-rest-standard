import { AuthGuard } from '@nestjs/passport';
import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthErrorEnum } from 'src/enums';
import { Observable } from 'rxjs';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // Call parent's canActivate method to handle authentication
    return super.canActivate(context);
  }

  handleRequest(err, user) {
    // If error or no user found, throw UnauthorizedException
    if (err || !user) {
      throw err || new UnauthorizedException(AuthErrorEnum.TOKEN_INVALID);
    }
    // User is authenticated, return the user
    return user;
  }
}
