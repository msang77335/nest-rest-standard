import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthService } from '../auth.service';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly authService: AuthService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());

    if (!roles) {
      return true;
    }

    // Extract the request from the context
    const request = context.switchToHttp().getRequest();

    // Extract the JWT token from the request headers or wherever it is stored
    const token = request.headers.authorization?.split(' ')[1];

    if (!token) {
      return false; // If no token is present, access is denied
    }

    const isVerify = await this.authService.verifyRoles(token, roles);

    return isVerify;
  }
}
