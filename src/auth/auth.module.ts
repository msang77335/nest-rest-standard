import { Global, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtStrategy, LocalStrategy } from './strategies';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { JwtEnums } from 'src/enums';
import { UserModule } from 'src/user/user.module';

@Global()
@Module({
  imports: [
    UserModule,
    JwtModule.registerAsync({
      useFactory: (config: ConfigService) => ({
        secret: config.get(JwtEnums.JWT_SECRE_ACCESS_TOKEN),
        signOptions: {
          expiresIn: config.get(JwtEnums.JWT_EXPIRESIN_ACCESS_TOKEN),
        },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
