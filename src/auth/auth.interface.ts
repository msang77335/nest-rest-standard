export interface Role {
  key: string;
  name: string;
}

export interface User {
  email: string;
  password: string;
  roles: Role[];
}

export interface LocalSign {
  email: string;
  password: string;
}

export interface JwtSign {
  accessToken: string;
  refresh_token: string;
}

export interface VerifyToken {
  accessToken: string;
}

export interface Auth {
  accessToken: string;
  roles: string[];
}

export interface JwtPayload {
  sub: string;
  email: string;
  roles: string[];
}

export interface JwtRefreshPayload {
  email: string;
}

export interface Payload {
  userId: string;
  email: string;
  roles: string[];
}
