import {
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/service/user.service';
import { Role, User } from './auth.interface';
import { LocalSignDTORes } from './auth.dto';
import { AuthErrorEnum, JwtEnums } from 'src/enums';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwt: JwtService,
    private readonly configService: ConfigService,
    private readonly userService: UserService,
  ) {}

  async validateUser(email: string, password: string): Promise<User | null> {
    const user = await this.userService.findByEmail(email).catch(() => {
      throw new UnauthorizedException(AuthErrorEnum.LOGIN_INFO_INCORRECT);
    });

    if (user && user.password === password) return user;

    throw new UnauthorizedException(AuthErrorEnum.LOGIN_INFO_INCORRECT);
  }

  async verifyRoles(token: string, roles: string[]): Promise<boolean> {
    try {
      // Decode the JWT token to get user data (assuming it contains user roles)
      const decodedToken = this.jwt.verify(token);

      // Extract roles from decoded token (assuming roles are stored in 'roles' property)
      const userRoles: Role[] = decodedToken.roles;

      // Check if any of the user roles match the roles allowed for the route
      const hasPermission = roles.some((role) =>
        userRoles.some((userRole) => userRole.key === role),
      );

      return hasPermission;
    } catch (error) {
      throw new ForbiddenException(AuthErrorEnum.FORBIDDEN_RESOURCE);
    }
  }

  public localSign(user: User): LocalSignDTORes {
    return {
      access_token: this.jwt.sign(user),
      refresh_token: this.getRefreshToken(user.email),
    };
  }

  private getRefreshToken(email: string): string {
    return this.jwt.sign(
      { email },
      {
        secret: this.configService.get(JwtEnums.JWT_SECRE_REFRESH_TOKEN),
        expiresIn: this.configService.get(JwtEnums.JWT_EXPIRESIN_REFRESH_TOKEN),
      },
    );
  }
}
