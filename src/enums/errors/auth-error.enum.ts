export enum AuthErrorEnum {
  TOKEN_INVALID = 'Token invalid',
  REFRESH_TOKEN_INVALID = 'Refresh token invalid',
  LOGIN_INFO_INCORRECT = 'Your login information was incorrect',
  FORBIDDEN_RESOURCE = 'Forbidden resource',
}
