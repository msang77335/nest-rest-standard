export enum AuthEnum {
  TOKEN_INVALID = 'Token invalid',
  REFRESH_TOKEN_INVALID = 'Refresh token invalid',
}
