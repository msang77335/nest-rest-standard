import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, isValidObjectId } from 'mongoose';
import { User, UserDocument } from '../user.schema';
import { CreateUserDTOReq, UserDTORes } from '../user.dto';
import { UserErrorEnum } from 'src/enums';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async findAll(): Promise<UserDTORes[]> {
    const users = await this.userModel.find().exec();
    return users.map((user) => user.toObject());
  }

  async findById(id: string): Promise<UserDTORes> {
    if (!isValidObjectId(id))
      throw new NotFoundException(UserErrorEnum.USER_NOT_FOUND);

    const user = await this.userModel.findById(id).exec();

    if (!user) throw new NotFoundException(UserErrorEnum.USER_NOT_FOUND);

    return user.toObject();
  }

  async findByEmail(email: string): Promise<UserDTORes> {
    const user = await this.userModel.findOne({ email }).exec();

    if (!user) {
      throw new NotFoundException(UserErrorEnum.USER_NOT_FOUND);
    }

    return user.toObject();
  }

  async create(createUserDtoReq: CreateUserDTOReq): Promise<UserDTORes> {
    const user = new User(createUserDtoReq);
    const createdUser = await new this.userModel(user).save();
    return createdUser.toObject();
  }

  async update(
    id: string,
    updateUserDto: Partial<CreateUserDTOReq>,
  ): Promise<UserDTORes> {
    const user = await this.findById(id);

    Object.assign(user, updateUserDto);
    user.updated_at = new Date();

    const updatedUser = await this.userModel
      .findByIdAndUpdate(id, user, { new: true })
      .exec();
    return updatedUser.toObject();
  }

  async delete(id: string): Promise<UserDTORes> {
    await this.findById(id);

    const deletedUser = await this.userModel.findByIdAndDelete(id).exec();
    return deletedUser.toObject();
  }
}
