import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { BaseSchema } from 'src/schema';

export type UserDocument = HydratedDocument<User>;

@Schema()
export class Role {
  @Prop({ required: true })
  key: string;

  @Prop({ required: true })
  name: string;
}

@Schema()
export class User extends BaseSchema {
  @Prop({ required: true })
  email: string;

  @Prop({ required: true })
  password: string;

  @Prop([Role])
  roles: Role[];

  constructor(user: Partial<User>) {
    super();
    Object.assign(this, user);
  }
}

export const UserSchema = SchemaFactory.createForClass(User);
