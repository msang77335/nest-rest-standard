import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseInterceptors,
} from '@nestjs/common';
import { UserService } from '../service/user.service';
import { CreateUserDTOReq, UserDTORes } from '../user.dto';
import { Serialize } from 'src/common/interceptors';
import { CacheInterceptor, CacheKey, CacheTTL } from '@nestjs/cache-manager';
import { CacheEnum } from 'src/enums';
import { Auth } from 'src/auth/decorators';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @Auth('admin')
  @Serialize(UserDTORes)
  @UseInterceptors(CacheInterceptor)
  @CacheKey(CacheEnum.ALL_USERS)
  @CacheTTL(10000)
  findAll(): Promise<UserDTORes[]> {
    return this.userService.findAll();
  }

  @Post()
  @Serialize(UserDTORes, CreateUserDTOReq)
  create(@Body() createUserDtoReq: CreateUserDTOReq): Promise<UserDTORes> {
    return this.userService.create(createUserDtoReq);
  }

  @Put(':id')
  @Serialize(UserDTORes, CreateUserDTOReq)
  update(
    @Param('id') id: string,
    @Body() updateUserDto: CreateUserDTOReq,
  ): Promise<UserDTORes> {
    return this.userService.update(id, updateUserDto);
  }

  @Delete(':id')
  @Serialize(UserDTORes)
  delete(@Param('id') id: string): Promise<UserDTORes> {
    return this.userService.delete(id);
  }
}
