import { Expose, Type } from 'class-transformer';
import {
  IsArray,
  IsEmail,
  IsNotEmpty,
  MinLength,
  NotContains,
  ValidateNested,
} from 'class-validator';
import { ObjectId } from 'mongoose';
import { BaseDTO } from 'src/dto';

export class RolseDTO {
  @IsNotEmpty()
  @Expose()
  key: string;

  @IsNotEmpty()
  @Expose()
  name: string;
}

export class CreateUserDTOReq {
  @IsNotEmpty()
  @IsEmail()
  @Expose()
  email: string;

  @IsNotEmpty()
  @Expose()
  @MinLength(6)
  @NotContains(' ')
  password: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => RolseDTO)
  @Expose()
  roles: RolseDTO[];
}

export class UserDTORes extends BaseDTO {
  @Expose({ name: '_id' })
  id: ObjectId;

  @Expose()
  email: string;

  @Type(() => RolseDTO)
  @Expose()
  roles: RolseDTO[];

  password: string;
}
